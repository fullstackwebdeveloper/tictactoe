<img src="/logo.png" alt="TicTacToe logo" style="height: 100px; width:100px;"/>

# Tic Tac Toe game

This is an implementation of a Tic Tac Toe game using Laravel/Blade.

- Authentication system allowing each user to create an account and play their own game.
- Each turn is against the server.
- Statistics page where user can see their statistics.
- Leaderboard to see where the user stands compared to other players.

## How to install

1. Clone the project
2. Go to the project directory and open a terminal
3. Run the following commands:
```shell
composer install
npm install
```
4. Duplicate the file .env.example and rename it .env
5. In the .env file, configure the connection to the database (lines 22 to 27)
For example:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tictactoe
DB_USERNAME=root
DB_PASSWORD=root
```
6. Make sure the database exists and is available
7. Run the following commands in the terminal:
```shell
php artisan key:generate
php artisan migrate
php artisan serve
```
8. In a separate terminal window, run:
```shell
npm run dev
```
9. Enjoy!


### License

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
