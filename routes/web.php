<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
use App\Http\Controllers\ProfileController;

Route::get('/', function () {
    return view('welcome');
});




Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/profile/statistics', [GameController::class, 'stats'])->name('profile.stats');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/game', [GameController::class, 'show'])->name('game.display');
    Route::post('/game', [GameController::class, 'play'])->name('game.play');
    Route::get('/leaderboard', [GameController::class, 'leaderboard'])->name('leaderboard');
});

require __DIR__.'/auth.php';
