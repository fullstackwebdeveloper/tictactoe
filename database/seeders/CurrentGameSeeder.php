<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CurrentGameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('currentgame')->insert(['id' => 1, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 2, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 3, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 4, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 5, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 6, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 7, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 8, 'state' => 0]);
        DB::table('currentgame')->insert(['id' => 9, 'state' => 0]);
    }
}
