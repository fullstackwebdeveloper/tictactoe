<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function show(){
        $game = $this->getCurrentGameStatus();

        if($this->isWinByClient($game)){
            return view("game", ["gameStatus" => $game, "isWinByClient" => true, "displayline" => $this->isWinByClient($game)]);
        }
        if($this->isWinByServer($game)){
            return view("game", ["gameStatus" => $game, "isWinByServer" => true, "displayline" => $this->isWinByServer($game)]);
        }

        if($this->isGameFinished()){
            return view("game", ["gameStatus" => $game, "isGameFinished" => true]);
        }
        else{
            return view("game", ["gameStatus" => $game]);
        }
    }

    public function play(Request $request){

        $game = $this->getCurrentGameStatus();
        
        if(isset($request->reset)){
            // DB::table('currentgame')->update(['state' => 0]);
            DB::table('currentgame')->where('user_id', Auth::id())->update(['gamestate' => "0,0,0,0,0,0,0,0,0"]);
            $game = $this->getCurrentGameStatus();
            return view("game", ["gameStatus" => $game]);
        }


        if(!isset($request->cell))
        {
            return view("game", ["gameStatus" => $game])->with("warning", "Please select a cell");
        }
        
        $cell = intval($request->cell);

        if($cell < 1 || $cell > 9){
            return view("game", ["gameStatus" => $game])->with("warning", "Cell index out of range");
        }
        
        
        if($game[$cell] > 0){
            return view("game", ["gameStatus" => $game])->with("warning", "That cell already has a value");
        }
        
        // DB::table('currentgame')->where('id', $cell)->update(['state' => 1]);
        $game[$cell] = 1;
        DB::table('currentgame')->where('user_id', Auth::id())->update(['gamestate' => join(",",$game->values()->toArray())]);
        $game = $this->getCurrentGameStatus();

        if($this->isWinByClient($game)){
            $userStats = DB::table('user_statistics')->where('user_id', Auth::id())->first();
            DB::table('user_statistics')->where('user_id', Auth::id())->update(['played' => $userStats->played + 1, 'win' => $userStats->win + 1]);

            return view("game", ["gameStatus" => $game, "isWinByClient" => true, "displayline" => $this->isWinByClient($game)]);
        }

        $this->computeServerTurn();
        $game = $this->getCurrentGameStatus();

        if($this->isWinByServer($game)){
            $userStats = DB::table('user_statistics')->where('user_id', Auth::id())->first();
            DB::table('user_statistics')->where('user_id', Auth::id())->update(['played' => $userStats->played + 1, 'lose' => $userStats->lose + 1]);
            return view("game", ["gameStatus" => $game, "isWinByServer" => true, "displayline" => $this->isWinByServer($game)]);
        }

        if($this->isGameFinished()){
            $userStats = DB::table('user_statistics')->where('user_id', Auth::id())->first();
            DB::table('user_statistics')->where('user_id', Auth::id())->update(['played' => $userStats->played + 1, 'draw' => $userStats->draw + 1]);
            return view("game", ["gameStatus" => $game, "isGameFinished" => true]);
        }

        return $this->show();
    }


    public function stats(){
        $userStats = DB::table('user_statistics')->where('user_id', Auth::id())->first();
        return view("user-stats", ["userstats" => $userStats]);
    }

    public function leaderboard(){
        $users = DB::table('user_statistics')->join('users', 'users.id', '=', 'user_statistics.user_id')->select('user_statistics.*', 'users.name')->orderBy('user_statistics.win', 'desc')->get();
        return view("leaderboard", ["users" => $users]);
    }

    private function getCurrentGameStatus(){
        $keys = collect([1,2,3,4,5,6,7,8,9]);
        $gamestate = str_getcsv(DB::table('currentgame')->where('user_id', Auth::id())->first()->gamestate, ',');
        return $keys->combine($gamestate);
    }

    private function computeServerTurn(){
        if($this->isGameFinished()){ return; }
        $game = $this->getCurrentGameStatus();

        // Win game if possible
        if($this->win($game)){ return; }

        // Block player if possible
        if($this->block($game)){ return; }
        
        // Else choose move from pre-computed moves
        if($this->precomputedMove($game)){ return; }
        
        // Else do random move
        if($this->randomMove($game)){ return; }



    }

    private function isGameFinished(){
        $game = $this->getCurrentGameStatus();
        return !$game->contains("0");
    }

    private function win($game){
        return $this->complete($game, 2);
    }

    private function block($game){
        return $this->complete($game, 1);
    }

    private function complete($game, $symbolInLine){
        if($this->completeLine($game, 1, 2, 3, $symbolInLine)){ return true; }
        if($this->completeLine($game, 4, 5, 6, $symbolInLine)){ return true; }
        if($this->completeLine($game, 7, 8, 9, $symbolInLine)){ return true; }
        if($this->completeLine($game, 1, 4, 7, $symbolInLine)){ return true; }
        if($this->completeLine($game, 2, 5, 8, $symbolInLine)){ return true; }
        if($this->completeLine($game, 3, 6, 9, $symbolInLine)){ return true; }
        if($this->completeLine($game, 1, 5, 9, $symbolInLine)){ return true; }
        if($this->completeLine($game, 3, 5, 7, $symbolInLine)){ return true; }
        return false;
    }

    private function completeLine($game, $a, $b, $c, $symbolInLine)
    {
        $line = $game[$a] . $game[$b] . $game[$c];
        switch($line){
            case "0" . $symbolInLine . $symbolInLine:
                // DB::table('currentgame')->where('id', $a)->update(['state' => 2]);
                $game[$a] = 2;
                DB::table('currentgame')->where('user_id', Auth::id())->update(['gamestate' => join(",",$game->values()->toArray())]);
                return true;
            case $symbolInLine . "0" . $symbolInLine:
                // DB::table('currentgame')->where('id', $b)->update(['state' => 2]);
                $game[$b] = 2;
                DB::table('currentgame')->where('user_id', Auth::id())->update(['gamestate' => join(",",$game->values()->toArray())]);
                return true;
            case $symbolInLine . $symbolInLine . "0":
                // DB::table('currentgame')->where('id', $c)->update(['state' => 2]);
                $game[$c] = 2;
                DB::table('currentgame')->where('user_id', Auth::id())->update(['gamestate' => join(",",$game->values()->toArray())]);
                return true;
        }
        return false;
    }

    private function precomputedMove($game)
    {
        // TODO : check if known situation
        return false;
    }

    private function randomMove(Collection $game)
    {
        $emptyCells = $game->filter(function (int $value, int $key) {
            return $value == 0;
        })->keys();
        if(count($emptyCells) > 0){
            $randomIndex = rand(0, count($emptyCells) - 1);
            // DB::table('currentgame')->where('id', $emptyCells[$randomIndex])->update(['state' => 2]);
            $index = $emptyCells[$randomIndex];
            $game[$index] = 2;
            DB::table('currentgame')->where('user_id', Auth::id())->update(['gamestate' => join(",",$game->values()->toArray())]);
            return true;
        }
        return false;
    }

    private function isWinByClient($game){
        return $this->isWin($game, 1);
    }

    private function isWinByServer($game){
        return $this->isWin($game, 2);
    }
    private function isWin($game, $val){
        if($this->isLineWin($game, 1, 2, 3, $val)){ return 1; }
        if($this->isLineWin($game, 4, 5, 6, $val)){ return 2; }
        if($this->isLineWin($game, 7, 8, 9, $val)){ return 3; }
        if($this->isLineWin($game, 1, 4, 7, $val)){ return 4; }
        if($this->isLineWin($game, 2, 5, 8, $val)){ return 5; }
        if($this->isLineWin($game, 3, 6, 9, $val)){ return 6; }
        if($this->isLineWin($game, 1, 5, 9, $val)){ return 7; }
        if($this->isLineWin($game, 3, 5, 7, $val)){ return 8; }
        return 0;
    }

    private function isLineWin($game, $a, $b, $c, $val){
        return $game[$a] == $val && $game[$b] == $val && $game[$c] == $val;
    }
}
