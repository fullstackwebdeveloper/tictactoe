<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserCurrentGameInDatabase
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(UserRegistered $event): void
    {
        DB::table('currentgame')->insert(['user_id' => $event->user->id, 'gamestate' => "0,0,0,0,0,0,0,0,0"]);
        DB::table('user_statistics')->insert(['user_id' => $event->user->id, 'played' => 0, 'win' => 0, 'lose' => 0, 'draw' => 0]);
    }
}
