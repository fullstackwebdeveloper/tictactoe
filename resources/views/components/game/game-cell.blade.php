<div class="relative w-[min(calc(100vw/3),calc(70vh/3))] h-[min(calc(100vw/3),calc(70vh/3))] border-2 border-black bg-gray-200">
    @if ($type == "0")
        <input onclick="submitForm()" class="opacity-0 w-full h-full absolute peer" type="radio" name="cell" value="{{$index}}" />
        <div class="w-full h-full peer-checked:bg-lime-500"></div>
    @elseif ($type == "1")
        <svg class="w-full h-full" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="#3fb4d1" d="M224 96a160 160 0 1 0 0 320 160 160 0 1 0 0-320zM448 256A224 224 0 1 1 0 256a224 224 0 1 1 448 0z"/></svg>
    @elseif ($type == "2")
        <svg class="w-full h-full" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="#d14e3f" d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"/></svg>
    @endif
</div>