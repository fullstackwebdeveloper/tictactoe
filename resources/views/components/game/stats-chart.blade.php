@php
    if($stats->played > 0){
        $winPercent = $stats->win / $stats->played;
        $drawPercent = $stats->draw / $stats->played;
        $losePercent = $stats->lose / $stats->played;
    }
    else{
        $winPercent = 0;
        $drawPercent = 0;
        $losePercent = 0;
    }

    $winStartPosX = 50;
    $winStartPosY = 0;
    $winAngle = $winPercent * 2 * M_PI;
    $winEndAngle = M_PI / 2 - $winAngle;
    $winEndPosX = cos($winEndAngle) * 50 + 50;
    $winEndPosY = (0-sin($winEndAngle)) * 50 + 50;
    $winLarge = $winAngle > M_PI ? 1 : 0;

    $loseStartPosX = $winEndPosX;
    $loseStartPosY = $winEndPosY;
    $loseAngle = $losePercent * 2 * M_PI;
    $loseEndAngle = $winEndAngle - $loseAngle;
    $loseEndPosX = cos($loseEndAngle) * 50 + 50;
    $loseEndPosY = (0-sin($loseEndAngle)) * 50 + 50;
    $loseLarge = $loseEndAngle > M_PI ? 1 : 0;

    $drawStartPosX = $loseEndPosX;
    $drawStartPosY = $loseEndPosY;
    $drawAngle = $drawPercent * 2 * M_PI;
    $drawEndAngle = $loseEndAngle - $drawAngle;
    $drawEndPosX = cos($drawEndAngle) * 50 + 50;
    $drawEndPosY = (0-sin($drawEndAngle)) * 50 + 50;
    $drawLarge = $drawEndAngle > M_PI ? 1 : 0;
    

@endphp

@if ($winPercent == 0 && $drawPercent == 0 && $losePercent == 0)
<p class="text-center text-gray-200">There is no graph to display</p>
@else
    <div class="w-[400px] h-[400px]">
        <svg class="" viewBox="0 0 100 100">

            @if ($winPercent == 1)
                <circle class="fill-[#78cf00] hover:fill-[#9bff82]" r="50%" cx="50%" cy="50%" > </circle>
            @elseif ($drawPercent == 1)
                <circle class="fill-[#ffe600] hover:fill-[#fdff6b]" r="50%" cx="50%" cy="50%" > </circle>
            @elseif ($losePercent == 1)
                <circle class="fill-[#e32605] hover:fill-[#ff7c6b]" r="50%" cx="50%" cy="50%" > </circle>
            @else
                <path class="fill-[#78cf00] stroke-[1px] stroke-black hover:fill-[#9bff82]" d="M {{$winStartPosX}} {{$winStartPosY}} A 50 50 0 {{$winLarge}} 1 {{$winEndPosX}} {{$winEndPosY}} L 50 50 Z" />
                <path class="fill-[#e32605] stroke-[1px] stroke-black hover:fill-[#ff7c6b]" d="M {{$loseStartPosX}} {{$loseStartPosY}} A 50 50 0 {{$loseLarge}} 1 {{$loseEndPosX}} {{$loseEndPosY}} L 50 50 Z" />
                <path class="fill-[#ffe600] stroke-[1px] stroke-black hover:fill-[#fdff6b]" d="M {{$drawStartPosX}} {{$drawStartPosY}} A 50 50 0 {{$drawLarge}} 1 {{$drawEndPosX}} {{$drawEndPosY}} L 50 50 Z" />
            @endif

            {{-- <circle class="fill-none stroke-black stroke-[.5%]" r="49.75%" cx="50%" cy="50%" > </circle> --}}
            {{-- <path class="fill-none stroke-black stroke-[.5%]" d="M 50 0 L 50 50" /> --}}
            {{-- <path class="fill-none stroke-black stroke-[.5%]" d="M 50 50 L {{$winEndPosX}} {{$winEndPosY}}" /> --}}
            {{-- <path class="fill-none stroke-black stroke-[.5%]" d="M 50 50 L {{$loseEndPosX}} {{$loseEndPosY}}" /> --}}
        
        </svg>
    </div>
    <div class="flex items-center mt-5">
        <svg class="w-[1em] h-[1em] mr-2" viewBox="0 0 10 10"><rect class="fill-[#78cf00]" width="10" height="10" x="0" y="0" rx="0" ry="0" /></svg>
        <p class="text-gray-200">Win</p>
    </div>
    <div class="flex items-center">
        <svg class="w-[1em] h-[1em] mr-2" viewBox="0 0 10 10"><rect class="fill-[#e32605]" width="10" height="10" x="0" y="0" rx="0" ry="0" /></svg>
        <p class="text-gray-200">Lose</p>
    </div>
    <div class="flex items-center">
        <svg class="w-[1em] h-[1em] mr-2" viewBox="0 0 10 10"><rect class="fill-[#ffe600]" width="10" height="10" x="0" y="0" rx="0" ry="0" /></svg>
        <p class="text-gray-200">Draw</p>
    </div>
@endif

<div class="flex flex-col items-center font-bold mt-5 text-gray-200">
    <p>Played: {{$stats->played}} game{{$stats->played > 1 ? "s" : ""}}</p>
    <p>Win: {{$stats->win}}  game{{$stats->win > 1 ? "s" : ""}} ({{round($winPercent, 4) * 100}}%)</p>
    <p>Lose: {{$stats->lose}}  game{{$stats->lose > 1 ? "s" : ""}} ({{round($losePercent, 4) * 100}}%)</p>
    <p>Draw: {{$stats->draw}}  game{{$stats->draw > 1 ? "s" : ""}} ({{round($drawPercent, 4) * 100}}%)</p>
</div>