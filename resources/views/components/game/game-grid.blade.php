<div class="w-[min(100vw,70vh)] h-[min(100vw,70vh)]">
    <form class="relative flex flex-wrap items-center justify-center" action="{{route('game.play')}}" method="post" id="gridForm">
        @csrf
        <div class="relative flex flex-wrap items-center justify-center">
            @for ($i = 1; $i <= 9; $i++)
                <x-game.game-cell index="{{$i}}" type="{{$game[$i]}}"/>
            @endfor
            @switch($displayline)
            @case(1)
                <svg class="absolute h-full w-full" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="m 20,50 260,0"/></svg>
                @break
            @case(2)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 20,150 H 280"/></svg>
                @break
            @case(3)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 20,250 H 280"/></svg>
                @break
            @case(4)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 50,20 V 280"/></svg>
                @break
            @case(5)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 150,20 V 280"/></svg>
                @break
            @case(6)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 250,20 V 280"/></svg>
                @break
            @case(7)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 20,20 L 280,280"/></svg>
                @break
            @case(8)
                <svg class="absolute h-full w-full"  viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg"><path style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-dasharray:none;stroke-opacity:1" d="M 280,20 L 20,280"/></svg>
                @break
                
        @endswitch
        </div>
        @if(!$winclient && !$winserver && !$gamefinished)
            {{-- <button class="mt-[2vh] px-5 py-2 font-bold bg-lime-500 rounded-full" type="submit" value="submit">Play</button> --}}
        @else
            <input class="hidden" type="checkbox" name="reset" checked hidden />
            <button class="mt-[2vh] px-5 py-2 font-bold bg-lime-500 rounded-full" type="submit" value="reset">Restart</button>
        @endif       
    </form>
    <script>
        function submitForm(){
            document.getElementById("gridForm").submit(); 
        }
    </script>
</div>