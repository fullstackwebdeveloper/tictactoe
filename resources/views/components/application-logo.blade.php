<svg viewBox="0 0 360 360" xmlns="http://www.w3.org/2000/svg" {{ $attributes }}>
         <rect width="360" height="360" x="0" y="0" rx="20" ry="20" fill="#ffffff" />
         <path
         style="fill:none;stroke:#838383;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 120,0 V 360"/>
      <path
         style="fill:none;stroke:#838383;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 240,0 V 360"/>
      <path
         style="fill:none;stroke:#838383;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 360,120 H 0"/>
      <path
         style="fill:none;stroke:#838383;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 360,240 H 0"/>
      <circle
         style="fill:none;stroke:#3fb4d1;stroke-width:20;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers stroke fill"
         cx="300"
         cy="60"
         r="45" />
      <circle
         style="fill:none;stroke:#3fb4d1;stroke-width:20;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers stroke fill"
         cx="60"
         cy="300"
         r="45" />
      <circle
         style="fill:none;stroke:#3fb4d1;stroke-width:20;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers stroke fill"
         id="circle1402"
         cx="300"
         cy="300"
         r="45" />
      <circle
         style="fill:none;stroke:#3fb4d1;stroke-width:20;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers stroke fill"
         cx="180"
         cy="180"
         r="45" />
      <path
         style="fill:none;stroke:#d14e3f;stroke-width:20;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 140,20 220,100 M 220,20 140,100" />
      <path
         style="fill:none;stroke:#d14e3f;stroke-width:20;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 140,260 220,340 M 220,260 140,340" />
      <path
         style="fill:none;stroke:#d14e3f;stroke-width:20;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 260,140 340,220 M 340,140 260,220" />
      <path
         style="fill:none;stroke:#000000;stroke-width:10;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         d="M 40,320 320,40"/>
</svg>
