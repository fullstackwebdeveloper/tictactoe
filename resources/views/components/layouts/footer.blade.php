<footer class="static bottom-0 flex items-center justify-center h-[5vh] w-[100vw] bg-gray-800 text-gray-200">
    &copy; 2024 Alexis Zampiero - All rights reserved
</footer>