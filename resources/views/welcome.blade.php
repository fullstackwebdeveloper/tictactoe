<x-guest-layout>

<div class="flex items-center justify-center gap-8 text-white">
    <a class="hover:font-bold hover:bg-gray-200 hover:text-black rounded-xl px-5 py-2" href="{{route('login')}}">Login</a>
    <a class="hover:font-bold hover:bg-gray-200 hover:text-black rounded-xl px-5 py-2" href="{{route('register')}}">Register</a>
</div>


</x-guest-layout>