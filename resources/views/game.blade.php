<x-app-layout>
    @if (isset($warning))
        {{$warning}}
    @endif

    @php
        $clientWin = isset($isWinByClient);
        $serverWin = isset($isWinByServer);
        $gameFinished = isset($isGameFinished);
        $line = isset($displayline) ? $displayline : -1;

        if($clientWin){
            echo "You win !";
        }
        else if($serverWin){
            echo "Server win !";
        }
        else if($gameFinished){
            echo "There is no winner !";
        }

    @endphp

    <x-game.game-grid :game="$gameStatus" :winclient="$clientWin" :winserver="$serverWin" :gamefinished="$gameFinished" :displayline="$line" />
</x-app-layout>