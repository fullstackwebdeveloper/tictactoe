<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-200 leading-tight">
            Leaderboard
        </h2>
    </x-slot>

    
    <div>
        <table class="bg-gray-800 text-gray-200 w-[95vw] md:w-[85vw] lg:w-[75vw] xl:w-[50vw] border-2 border-gray-400 text-center">
            <tr class="border-[1px] border-gray-400">
                <th class="border-[1px] border-gray-400">Username</th>
                <th class="border-[1px] border-gray-400">Played</th>
                <th class="border-[1px] border-gray-400">Win</th>
                <th class="border-[1px] border-gray-400">Lose</th>
                <th class="border-[1px] border-gray-400">Draw</th>
            </tr>
        @foreach ($users as $user)
            <tr class="even:bg-gray-700">
                <td class="border-[1px] border-gray-400">{{$user->name}}</td>
                <td class="border-[1px] border-gray-400">{{$user->played}}</td>
                <td class="border-[1px] border-gray-400">{{$user->win}}</td>
                <td class="border-[1px] border-gray-400">{{$user->lose}}</td>
                <td class="border-[1px] border-gray-400">{{$user->draw}}</td>
            </tr>
        @endforeach
        </table>
    </div>

</x-app-layout>
