<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-200 leading-tight">
            Statistics
        </h2>
    </x-slot>

    <x-game.stats-chart :stats="$userstats" />

</x-app-layout>
